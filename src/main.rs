use bevy::{
    prelude::*,
    render::camera::{ScalingMode, WindowOrigin},
};

const WINDOW_WIDTH: f32 = 1024.0;
const WINDOW_HEIGHT: f32 = 1024.0 / 16.0 * 9.0;

fn main() {
    App::new()
        .insert_resource(WindowDescriptor {
            title: "Sticky Bridges".to_string(),
            width: WINDOW_WIDTH,
            height: WINDOW_HEIGHT,
            ..default()
        })
        .add_startup_system(setup_camera)
        .add_plugins(DefaultPlugins)
        .run();
}

fn setup_camera(mut commands: Commands) {
    commands.spawn_bundle(Camera2dBundle {
        projection: OrthographicProjection {
            window_origin: WindowOrigin::BottomLeft,
            scaling_mode: ScalingMode::FixedVertical(WINDOW_HEIGHT),
            ..default()
        },
        ..default()
    });
}
